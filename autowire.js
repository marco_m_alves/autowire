angular.module('autowire', [])
    .directive('autowire', function ($compile, $templateCache, $interpolate, $http) {
        'use strict';
        
        var getTemplateUrl, getControllerName, getSetExpression,
            bindToParent, renderAndCompile, specialAttrs,
            getAttrsToWatch, setWatchesForAttrs, linkingFn;
        
        specialAttrs = { 'autowire': undefined, 'path': undefined };
        
        String.prototype.dashToCamel = function () {
            return this.replace(/(\-[a-z])/g, function ($1) {return $1.toUpperCase().replace('-', ''); });
        };
        
        String.prototype.capitalize = function () {
            return this.charAt(0).toUpperCase() + this.substring(1);
        };
        
        getControllerName = function (templateDashName) {
            return templateDashName.dashToCamel().capitalize();
        };
        
        getTemplateUrl = function (tag, path) {
            if (path) {
                return path + '/' + tag + '.html';
            } else {
                return tag + '.html';
            }
        };
        
        bindToParent = function (scope, localVar, parentVar) {
            //console.log('wiring parent-child binding for:', localVar, parentVar);
            
            var directiveScope = scope,
                parentScope = scope.$parent,
                childScope = scope.$$childHead;
            
            localVar = localVar.dashToCamel();
            
            parentScope.$watch(parentVar, function () {
                // console.log('parent watch');
                var terms = { a: localVar, b: parentVar };
                var localExpression = $interpolate('$$childHead.{{a}} = $parent.{{b}}')(terms);
                console.log('evaluating', localExpression, 'in scope');
                directiveScope.$eval(localExpression);
            }, true);
            
            childScope.$watch(localVar, function () {
                // console.log('local watch');
                var terms = { a: parentVar, b: localVar };
                var parentExpression = $interpolate('$parent.{{a}} = $$childHead.{{b}}')(terms);
                console.log('evaluating', parentExpression, 'in scope');
                directiveScope.$eval(parentExpression);
            }, true);
        };
        
        
        getAttrsToWatch = function (attrs) {
            var attrsToWatch = [];
            
            angular.forEach(attrs, function (value, attr) {
                if (!(specialAttrs.hasOwnProperty(attr)) && attr.charAt(0) !== '$') {
                    attrsToWatch.push({ localVar: attr, parentVar: value });
                }
            });
            
            return attrsToWatch;
        };
        
        setWatchesForAttrs = function (scope, attrs) {
            angular.forEach(attrs, function (attr) {
                bindToParent(scope, attr.localVar, attr.parentVar);
            });
        };
        
        renderAndCompile = function (params) {
            var template, tagName = params.tagName, content = params.content, element = params.element,
                controllerName = params.controllerName, container, scope = params.scope,
                attrs = params.attrs, attrsToWatch;
            
            template = '<div ng-controller="{{ctrl}}">{{content}}</div>';
            template = template.replace(/\{\{ctrl\}\}/g, controllerName);
            template = content ? template.replace(/\{\{content\}\}/g, content) : template;
            
            container = angular.element(template);
            element.append(container);
            $compile(container)(scope);
            
            attrsToWatch = getAttrsToWatch(attrs);
            setWatchesForAttrs(scope, attrsToWatch);
        };
        
        linkingFn = function (scope, element, attrs) {
            var tagName, templateUrl, controllerName, content, invokeRenderAndCompile;
                    
            tagName = element[0].localName;
            controllerName = getControllerName(tagName);
            content = $templateCache.get(tagName);
            
            invokeRenderAndCompile = function (content) {
                renderAndCompile({
                    element: element,
                    tagName: tagName,
                    controllerName: controllerName,
                    content: content,
                    scope: scope,
                    attrs: attrs
                });
            };
            
            
            if (content) {
                invokeRenderAndCompile(content);
            } else {
                templateUrl = getTemplateUrl(tagName, attrs.path);
                $http.get(templateUrl).success(function (response) {
                    invokeRenderAndCompile(response);
                });
            }
        };
        
        return {
            scope: {}, // ensures isolated scope
            link: linkingFn // handles template loading and parent-child sync
        };
    });