angular.module('App', ['autowire']).
    controller('Test', function ($scope) {
        'use strict';
        
        $scope.controllerLocalVar = 'controllerLocalVar';
    });

describe('autowire directive', function () {
    'use strict';
    
    var $compile, $rootScope, $templateCache, $controller, $rootElement, template;
    
    beforeEach(module('App'));
    
    beforeEach(inject(
        ['$compile', '$rootScope', '$templateCache', '$controller', '$rootElement', function ($c, $r, $t, $ctrl, $e) {
            $compile = $c;
            $rootScope = $r;
            $templateCache = $t;
            $controller = $ctrl;
            $rootElement = $e;
        }]
    ));
    
    it('should compile and bind without errors', function () {
        var template = '<div>{{name}}</div>', element;
        $templateCache.put('test', template);
        
        element = $compile('<test autowire name="testName"></test>')($rootScope);
        $rootScope.$digest(); // forces bindings to work
    });
    
    it('should populate the directive inner html with controller and template content', function () {
        var template = '<div>{{name}}</div>', element;
        $templateCache.put('test', template);
        
        element = $compile('<test autowire name="testName"></test>')($rootScope);
        
        expect(element.html()).toContain('ng-controller="Test"');
        expect(element.html()).toContain('{{name}}');
    });
    
    it('should create 2 additional scopes, one of them being an isolate scope', function () {
        var template = '<div>{{name}}</div>', element;
        $templateCache.put('test', template);
        
        element = $compile('<test autowire name="testName"></test>')($rootScope);
        $rootElement.append(element);
        
        expect($($rootElement).find('.ng-scope').length).toEqual(2);
        expect($($rootElement).find('.ng-isolate-scope').length).toEqual(1);
    });
    
    it('should reflect changes in the outer scope to changes in the inner scope', function () {
        var template = '<div>{{name}}</div>',
            element,
            innerScope;
        
        $templateCache.put('test', template);
        element = $compile('<test autowire name="testName"></test>')($rootScope);
        $rootScope.$digest(); // forces bindings to work
        
        innerScope = $rootScope.$$childHead.$$childHead;
        $rootScope.$apply(function () {
            $rootScope.testName = 'angular';
        });
        
        expect(innerScope.name).toContain('angular');
    });
    
    it('should reflect changes in the inner scope to changes in the outer scope', function () {
        var template = '<div>{{name}}</div>',
            element,
            innerScope;
        
        $templateCache.put('test', template);
        element = $compile('<test autowire name="testName"></test>')($rootScope);
        $rootScope.$digest(); // forces bindings to work
        
        innerScope = $rootScope.$$childHead.$$childHead;
        innerScope.$apply(function () {
            innerScope.name = 'angular';
        });
        
        expect($rootScope.testName).toContain('angular');
    });
    
    
    it('should bind correctly even if parent var has dashes ', function () {
        var template = '<div>{{name}}</div>',
            element,
            innerScope;
        
        $templateCache.put('test', template);
        element = $compile('<test autowire name-text="testName"></test>')($rootScope);
        $rootScope.$digest(); // forces bindings to work
        
        // check inner --> outer
        innerScope = $rootScope.$$childHead.$$childHead;
        innerScope.$apply(function () {
            innerScope.nameText = 'angular';
        });
        
        expect($rootScope.testName).toContain('angular');
        
        
        // check outer --> inner
        $rootScope.$apply(function () {
            $rootScope.testName = 'dashed';
        });
        expect(innerScope.nameText).toContain('dashed');
    });

    it('shoud bind correctly with complex parent vars', function () {
        
        var template = '<div>{{name}}</div>',
            element,
            innerScope;
        
        $templateCache.put('test', template);
        element = $compile('<test autowire name-text="data.prop1.prop2"></test>')($rootScope);
        $rootScope.$digest(); // forces bindings to work
        
        // check inner --> outer
        innerScope = $rootScope.$$childHead.$$childHead;
        innerScope.$apply(function () {
            innerScope.nameText = 'angular';
        });
        
        expect($rootScope.data.prop1.prop2).toContain('angular');
        
        
        // check outer --> inner
        $rootScope.$apply(function () {
            $rootScope.data.prop1.prop2 = 'dashed';
        });
        expect(innerScope.nameText).toContain('dashed');
       
    });


    it('shoud correctly bind objects', function () {
        var template = '<div>{{name}}</div>',
            element,
            innerScope;
        
        $templateCache.put('test', template);
        element = $compile('<test autowire name="testName"></test>')($rootScope);
        $rootScope.$digest(); // forces bindings to work
        innerScope = $rootScope.$$childHead.$$childHead;

        // check outer --> inner
        $rootScope.$apply(function () {
            $rootScope.testName = {a: 'angular', b: 'is', c: 'great' };
        });
        expect(innerScope.name).toEqual({a: 'angular', b: 'is', c: 'great' });

        // check inner --> outer
        innerScope.$apply(function () {
            innerScope.name = {a: 'angular', b: 'is', c: 'awesome' };
        });
        expect($rootScope.testName).toEqual({a: 'angular', b: 'is', c: 'awesome' });
    });


    it('shoud correctly bind arrays', function () {
        var template = '<div>{{names}}</div>',
            element,
            innerScope;
        
        $templateCache.put('test', template);
        element = $compile('<test autowire names="testNames"></test>')($rootScope);
        $rootScope.$digest(); // forces bindings to work
        innerScope = $rootScope.$$childHead.$$childHead;

        // check outer --> inner
        $rootScope.$apply(function () {
            $rootScope.testNames = ['angular', 'is', 'great'];
        });
        expect(innerScope.names).toEqual(['angular', 'is', 'great']);

        // check inner --> outer
        innerScope.$apply(function () {
            innerScope.names = ['angular', 'is', 'awesome'];
        });
        expect($rootScope.testNames).toEqual(['angular', 'is', 'awesome']);
    });
    
    
    it('shoud make controller set local vars available to the local scope but not bound to the parent scope', function () {
        var template = '<div>{{name}}</div>',
            element,
            innerScope;
        
        $templateCache.put('test', template);
        element = $compile('<test autowire name="testName"></test>')($rootScope);
        $rootScope.$digest(); // forces bindings to work
        
        innerScope = $rootScope.$$childHead.$$childHead;
        
        expect(innerScope.controllerLocalVar).toEqual('controllerLocalVar');
        
        innerScope.$apply(function () {
            innerScope.controllerLocalVar = 'angular';
        });
        
        expect(innerScope.controllerLocalVar).toEqual('angular');
        expect($rootScope.controllerLocalVar).toBeUndefined();
    });
    
    it('shoud prevent parent scope vars that are not passed as directive attribute values to be present in the local scope', function () {
        var template = '<div>{{name}}</div>',
            element,
            innerScope;
        
        $templateCache.put('test', template);
        element = $compile('<test autowire name="testName"></test>')($rootScope);
        $rootScope.$digest(); // forces bindings to work
        
        innerScope = $rootScope.$$childHead.$$childHead;
        
        innerScope.$apply(function () {
            $rootScope.parentScopeVar = 'parentScopeVar';
        });
        
        expect(innerScope.parentScopeVar).toBeUndefined();
        expect($rootScope.parentScopeVar).toEqual('parentScopeVar');
    });

});



