var app = angular.module('angularjs-starter', ['autowire']);

app.controller('MainCtrl', function ($scope) {
    'use strict';
    $scope.name = 'World';
  
    $scope.cont = 'details';

});


app.controller('ClientDetails', function ($scope) {
    'use strict';
    $scope.name = 'Marco';
    $scope.reset = function () {
        $scope.client = undefined;
    };
});